import cv2
import numpy as np
import matplotlib.pyplot as plt
import os
# Submit:
# Naor Matalon - 300471208
# Shmuel Levinson - 314467697


# Instruction:
# 1. You need to create two directories and put its path into "INPUT_FRAMES" and "OUTPUT_FRAMES".
# 2. Then, place your video file in "INPUT_FRAMES" directory, change the file name in "FILE_NAME", and call the
# function "break_video_to_frames()"
# 3. Remove the video file from the directory and make sure just the frames from the video are in there
# 4. Run the function "remove_specularity()"

FILE_NAME = "light_movement.mp4"
INPUT_FRAMES = 'images_dir'
OUTPUT_FRAMES = 'results'

# Hyper Parameters (after a lot of experiments)
ALPHA = -0.5
K = 1
BETA = 42
THETA = 0.1
GAMMA = 2.2


def break_video_to_frames():
    vidcap = cv2.VideoCapture("images_dir/" + str(FILE_NAME))
    success, image = vidcap.read()
    counter = 0
    success = True
    while success:
        cv2.imwrite(os.path.join("images_dir", "frame%d.jpg" % counter), image)  # save frame as JPEG file
        success, image = vidcap.read()
        counter += 1


def remove_specularity():
    rgb_images = []
    images_red, images_green, images_blue = [], [], []
    input_frames_paths = np.sort(np.array(os.listdir(INPUT_FRAMES)))
    for image_path in input_frames_paths:
        im_path = os.path.join(INPUT_FRAMES, image_path)
        frame_image = cv2.imread(im_path)

        # apply gamma correction using lookup table
        inverse_gamma = 1.0 / GAMMA
        look_up_table = np.array([((i / 255.0) ** inverse_gamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
        after_gamma_correction = cv2.LUT(frame_image, look_up_table)

        # performing white balance
        after_white_bal = cv2.cvtColor(after_gamma_correction, cv2.COLOR_BGR2LAB)
        avg_a = np.average(after_white_bal[:, :, 1])
        avg_b = np.average(after_white_bal[:, :, 2])
        after_white_bal[:, :, 1] = after_white_bal[:, :, 1] - ((avg_a - 128) * (after_white_bal[:, :, 0] / 255.0) * 1.1)
        after_white_bal[:, :, 2] = after_white_bal[:, :, 2] - ((avg_b - 128) * (after_white_bal[:, :, 0] / 255.0) * 1.1)
        after_white_bal = cv2.cvtColor(after_white_bal, cv2.COLOR_LAB2BGR)

        # normalizing
        after_white_bal = cv2.normalize(after_white_bal, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX,
                                        dtype=cv2.CV_32F)
        img = after_white_bal[:, :, ::-1]
        rgb_images.append(img)
        images_red.append(img[:, :, 0])
        images_green.append(img[:, :, 1])
        images_blue.append(img[:, :, 2])

    # computing min Jcs
    min_j_image = np.zeros(rgb_images[0].shape)
    min_j_image[:, :, 0] = np.amin(np.stack(images_red, axis=2), axis=2)
    min_j_image[:, :, 1] = np.amin(np.stack(images_green, axis=2), axis=2)
    min_j_image[:, :, 2] = np.amin(np.stack(images_blue, axis=2), axis=2)

    specularity_map = []
    for i in range(len(rgb_images)):
        specularity_map.append(rgb_images[i] - min_j_image)

    # Video amplification
    fixed_images = []
    for x in specularity_map:
        fixed_image = min_j_image + x * (K + ALPHA * (1 / (1 + np.exp((-BETA) * (x - THETA)))))
        fixed_images.append(fixed_image)

    # saving the plots
    for i in range(len(rgb_images)):
        plt.figure()
        plt.subplot(1, 4, 1)
        plt.axis("off")
        plt.title("Original")
        plt.imshow(rgb_images[i])

        plt.subplot(1, 4, 2)
        plt.axis("off")
        plt.title("Processed")
        plt.imshow(fixed_images[i])

        plt.subplot(1, 4, 3)
        plt.axis("off")
        plt.title("Time-Cumulative")
        plt.imshow(specularity_map[i])
        plt.subplot(1, 4, 4)
        plt.axis("off")
        plt.title("Difference")
        dif = rgb_images[i] - fixed_images[i]
        plt.imshow(dif)
        plt.savefig(str(OUTPUT_FRAMES) + "/frame" + str(i) + ".png", dpi=150)
        plt.close()


if __name__ == "__main__":
    # break_video_to_frames()
    remove_specularity()
